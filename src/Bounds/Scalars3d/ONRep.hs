{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE LambdaCase             #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE MultiWayIf             #-}
{-# LANGUAGE PolyKinds              #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE RecordWildCards        #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE StaticPointers         #-}
{-# LANGUAGE TupleSections          #-}
{-# LANGUAGE TypeApplications       #-}
{-# LANGUAGE TypeFamilies           #-}
{-# LANGUAGE TypeOperators          #-}

module Bounds.Scalars3d.ONRep where

import           Data.Aeson                             (FromJSON, ToJSON)
import           Data.Binary                            (Binary)
import           Data.Proxy                             (Proxy (..))
import           Data.Reflection                        (Reifies, reflect)
import           GHC.Generics                           (Generic)
import           Blocks                            (Block (..))
import           Bootstrap.Math.FreeVect                     (FreeVect, vec)
import qualified Bootstrap.Math.FreeVect                     as FV
import           Bootstrap.Math.VectorSpace                  ((*^))

------------------ O(N) representation theory ---------------------

-- | Here, we only implement the O(N) representation theory needed for
-- this problem, which involves four-point functions of O(N) singlets
-- and vectors.

-- | O(N) representations appearing in correlators of singlets and
-- vectors
data ONRep = ONSinglet | ONVector | ONSymTensor | ONAntiSymTensor
  deriving (Show, Eq, Ord, Generic, Binary, ToJSON, FromJSON,Bounded,Enum)

-- | Flavor structures for a four-point function of SO(N) vectors,
-- from Aike's notes. Here v_{ij} = v_i.v_j with v_j an SO(N)
-- polarization vector:
--
--  QPlus  = v_{12} v_{34} + v_{23} v_{14} (crossing even)
--  QMinus = v_{12} v_{34} - v_{23} v_{14} (crossing odd)
--  Q3     = v_{13} v_{24}                 (crossing even)
--
--  Unique plays two roles: as v_i.v_j in a four-point function
--  containing two vector's, and as 1 in a four-point function of
--  singlets.
data ON4PtStruct n = QPlus | QMinus | Q3 | Unique
  deriving (Eq, Ord)

-- | For the ONReps we allow here, there is a unique three-point
-- structure for each allowed triplet of representations. Thus, the
-- 3-point structures are just labeled by the representations.
data ON3PtStruct n = ON3PtStruct ONRep ONRep ONRep
  deriving (Eq, Ord)

-- | Flavor block for the exchange of the given 'ONRep' in a
-- four-point function of O(N) vectors.
onVectorBlock
  :: forall n a . (Reifies n Rational, Fractional a, Eq a)
  => ONRep
  -> FreeVect (ON4PtStruct n) a
onVectorBlock r = case r of
  ONSinglet       -> v12_34
  ONSymTensor     -> 1/2 *^ (v13_24 + v23_14) - 1/nGroup *^ v12_34
  -- | Fixed a sign here, since the block should come from merging
  -- (120) and (430) -- double check this!
  ONAntiSymTensor -> 1/2 *^ (v23_14 - v13_24)
  _               -> 0
  where
    -- | vij_kl represents v_{ij} v_{kl}. Here, they can be obtained
    -- by inverting the definitions of QPlus, QMinus, Q3.
    v12_34 = 1/2 *^ vec QPlus + 1/2 *^ vec QMinus
    v23_14 = 1/2 *^ vec QPlus - 1/2 *^ vec QMinus
    v13_24 = vec Q3
    nGroup = fromRational (reflect @n Proxy)

-- | Evaluate the given O(N) flavor block to obtain a number
evalONBlock :: (Reifies n Rational, Fractional a, Eq a) => Block (ON3PtStruct n) (ON4PtStruct n) -> a
evalONBlock (Block (ON3PtStruct r1 r2 r) (ON3PtStruct r4 r3 r') f)
  | r /= r' = 0
  | otherwise = case ((r1, r2, r3, r4), r, f) of
      ((ONVector, ONVector, ONVector, ONVector), _, _) -> FV.coeff f (onVectorBlock r)
      ((ONSinglet, ONSinglet, ONSinglet, ONSinglet), ONSinglet, Unique) -> 1
      ((ONVector,  ONVector,  ONSinglet, ONSinglet), ONSinglet, Unique) -> 1
      ((ONSinglet, ONSinglet, ONVector,  ONVector),  ONSinglet, Unique) -> 1
      ((ONVector,  ONSinglet, ONVector,  ONSinglet), ONVector,  Unique) -> 1
      ((ONVector,  ONSinglet, ONSinglet, ONVector),  ONVector,  Unique) -> 1
      ((ONSinglet, ONVector,  ONVector,  ONSinglet), ONVector,  Unique) -> 1
      ((ONSinglet, ONVector,  ONSinglet, ONVector),  ONVector,  Unique) -> 1
      _ -> 0
