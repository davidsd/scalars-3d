{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE RecordWildCards       #-}

module Projects.Scalars3d.ONVec2021Affine where

import           Bootstrap.Math.Linear              (toV)
import           Bootstrap.Math.AffineTransform (AffineTransform (..))

onVecAffineNgroup2Nmax6 :: AffineTransform 2 Rational
onVecAffineNgroup2Nmax6 = AffineTransform
  { affineShift  = toV ( 0.52, 1.51 )
  , affineLinear = toV ( toV (0.1, 0)
                       , toV (0, 0.2)
                       )
  }

onVecAffineNgroup2Nmax10 :: AffineTransform 2 Rational
onVecAffineNgroup2Nmax10 = AffineTransform
  { affineShift  = toV (0.5197, 1.5160)
  , affineLinear = toV ( toV (0.0015, 0.013)
                       , toV (-0.0008,  0.0002)
                       )
  }

-- these are good defaults, but for interoperability, let's turn affine into a function
-- first param is ngroup, second is nmax
onVecAffineGen :: Rational -> Int -> AffineTransform 2 Rational
onVecAffineGen 2 6 = onVecAffineNgroup2Nmax6
onVecAffineGen 2 10 = onVecAffineNgroup2Nmax10

onVecAffineGen 3 6 = AffineTransform
  { affineShift  = toV ( 0.519, 1.595 )
  , affineLinear = toV ( toV (0.05, 0)
                       , toV (0, 0.05)
                       )
  }

onVecAffineGen 14 6 = AffineTransform
  { affineShift  = toV ( 0.5094, 1.940 )
  , affineLinear = toV ( toV (0.0022, 0)
                       , toV (0.0005, 0.06)
                       )
  }

onVecAffineGen 15 6 = AffineTransform
  { affineShift  = toV ( 0.509, 1.960 )
  , affineLinear = toV ( toV (0.0012, 0.0 )
                       , toV (0.0,    0.06)
                       )
  }

onVecAffineGen 16 6 = AffineTransform
  { affineShift  = toV ( 0.5086, 1.965 )
  , affineLinear = toV ( toV (0.0012, 0.0 )
                       , toV (0.0,    0.06)
                       )
  }

onVecAffineGen 17 6 = AffineTransform
  { affineShift  = toV (0.5078, 1.955)
  , affineLinear = toV ( toV (0.0012, 0.0 )
                       , toV (0.0004, 0.06)
                       )
  }

onVecAffineGen 18 6 = AffineTransform
  { affineShift  = toV ( 0.508, 1.963 )
  , affineLinear = toV ( toV (0.001,  0.0 )
                       , toV (0.0012, 0.06)
                       )
  }

onVecAffineGen 19 6 = AffineTransform
  { affineShift  = toV ( 0.5074, 1.963 )
  , affineLinear = toV ( toV (0.0012, 0)
                       , toV (0.001,  0.06)
                       )
  }

onVecAffineGen 20 6 = AffineTransform
  { affineShift  = toV ( 0.5064, 1.938 )
  , affineLinear = toV ( toV (0.001, 0)
                       , toV (0, 0.015)
                       )
  }

onVecAffineGen 20 8 = AffineTransform
  { affineShift  = toV ( 0.50667, 1.951 )
  , affineLinear = toV ( toV (0.00025, 0)
                       , toV (0.00035, 0.018)
                       )
  }

onVecAffineGen 3 10 = AffineTransform
  { affineShift  = toV ( 0.5189,1.592 )
  , affineLinear = toV ( toV (0.0036, 0.039)
                       , toV (-0.00135,  0.0001)
                       )
  }

onVecAffineGen 4 10 = AffineTransform
  { affineShift  = toV ( 0.51913555, 1.67043438 )
  , affineLinear = toV ( toV ( 0.00240114, 0.02591119 )
                       , toV ( -0.00090192, 0.00006658 )
                       )
  }

onVecAffineGen 5 10 = AffineTransform
  { affineShift  = toV ( 0.51787312, 1.72495114 )
  , affineLinear = toV ( toV ( 0.00201007, 0.02632994 )
                       , toV ( -0.00080835, 0.00006171 )
                       )
  }

onVecAffineGen 6 10 = AffineTransform
  { affineShift  = toV ( 0.51682359, 1.77235611 )
  , affineLinear = toV ( toV ( 0.00199019, 0.02740269 )
                       , toV ( -0.00080701, 0.00005861 )
                       )
  }

onVecAffineGen 7 10 = AffineTransform
  { affineShift  = toV ( 0.51547867, 1.80674923 )
  , affineLinear = toV ( toV ( 0.00173919, 0.02708512 )
                       , toV ( -0.00072206, 0.00004636 )
                       )
  }

onVecAffineGen 8 10 = AffineTransform
  { affineShift  = toV ( 0.51413674, 1.83249125 )
  , affineLinear = toV ( toV ( 0.00180914, 0.03076152 )
                       , toV ( -0.00060162, 0.00003538 )
                       )
  }

onVecAffineGen 9 10 = AffineTransform
  { affineShift  = toV ( 0.51305640, 1.85448615 )
  , affineLinear = toV ( toV ( 0.00141265, 0.02656807 )
                       , toV ( -0.00052161, 0.00002773 )
                       )
  }

onVecAffineGen 10 10 = AffineTransform
  { affineShift  = toV ( 0.51203146, 1.87112921 )
  , affineLinear = toV ( toV ( 0.00115891, 0.02347173 )
                       , toV ( -0.00045003, 0.00002222 )
                       )
  }

onVecAffineGen 11 10 = AffineTransform
  { affineShift  = toV ( 0.51116913, 1.88557074 )
  , affineLinear = toV ( toV ( 0.00087955, 0.02040789 )
                       , toV ( -0.00037215, 0.00001604 )
                       )
  }

onVecAffineGen 12 10 = AffineTransform
  { affineShift  = toV ( 0.51040605, 1.89755501 )
  , affineLinear = toV ( toV ( 0.00071812, 0.01731504 )
                       , toV ( -0.00031347, 0.00001300 )
                       )
  }

onVecAffineGen 13 10 = AffineTransform
  { affineShift  = toV ( 0.50966059, 1.90574335 )
  , affineLinear = toV ( toV ( 0.00058693, 0.01515195 )
                       , toV ( -0.00025795, 0.00000999 )
                       )
  }

onVecAffineGen 14 10 = AffineTransform
  { affineShift  = toV ( 0.50910156, 1.91424122 )
  , affineLinear = toV ( toV ( 0.00068362, 0.01593908 )
                       , toV ( -0.00029407, 0.00000969 )
                       )
  }

onVecAffineGen 15 10 = AffineTransform
  { affineShift  = toV ( 0.50856116, 1.92121013 )
  , affineLinear = toV ( toV ( 0.00058688, 0.01482596 )
                       , toV ( -0.00025690, 0.00000777 )
                       )
  }

onVecAffineGen 16 10 = AffineTransform
  { affineShift  = toV ( 0.50799889, 1.92570091 )
  , affineLinear = toV ( toV ( 0.00034952, 0.01114145 )
                       , toV ( -0.00015705, 0.00000493 )
                       )
  }

onVecAffineGen 17 10 = AffineTransform
  { affineShift  = toV ( 0.50755453, 1.93052273 )
  , affineLinear = toV ( toV ( 0.00031951, 0.01036145 )
                       , toV ( -0.00013718, 0.00000423 )
                       )
  }

onVecAffineGen 18 10 = AffineTransform
  { affineShift  = toV ( 0.50720041, 1.93539892 )
  , affineLinear = toV ( toV ( 0.00035995, 0.01038017 )
                       , toV ( -0.00020847, 0.00000549 )
                       )
  }

onVecAffineGen 19 10 = AffineTransform
  { affineShift  = toV ( 0.50684688, 1.94009004 )
  , affineLinear = toV ( toV ( 0.00023899, 0.00789534 )
                       , toV ( -0.00011011, 0.00000333 )
                       )
  }

onVecAffineGen 20 10 = AffineTransform
  { affineShift  = toV ( 0.50649544, 1.94239349 )
  , affineLinear = toV ( toV ( 0.00020646, 0.00814360 )
                       , toV ( -0.00009526, 0.00000242 )
                       )
  }

onVecAffineGen 30 _ = AffineTransform
  { affineShift  = toV ( 0.50442801, 1.96474121 )
  , affineLinear = toV ( toV ( 0.00013228, 0.00884445 )
                       , toV ( -0.00006816, 0.00000102 )
                       )
  }

onVecAffineGen 40 _ = AffineTransform
  { affineShift  = toV ( 0.50334237, 1.97444684 )
  , affineLinear = toV ( toV ( 0.00006488, 0.00590877 )
                       , toV ( -0.00003669, 0.00000040 )
                       )
  }

onVecAffineGen ngroup 12 = onVecAffineGen ngroup 10

onVecAffineGen _ _ = AffineTransform
  { affineShift  = toV ( 0.5, 1.5 )
  , affineLinear = toV ( toV (0.05, 0)
                       , toV (0, 0.05)
                       )
  }
