{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedRecordDot   #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeFamilies          #-}

module Projects.Scalars3d.SingletScalarBlocks3d2020 where

import System.FilePath.Posix (takeExtension, (</>))
import System.Directory (listDirectory)
import Blocks.Blocks3d                        qualified as B3d
import Blocks.ScalarBlocks                    (ScalarBlockParams (..))
import Bootstrap.Bounds                       (BoundDirection (..))
import Bootstrap.Bounds.Spectrum              (setGap, unitarySpectrum)
import Bounds.Scalars3d.SingletScalarBlocks3d (SingletScalar (..))
import Bounds.Scalars3d.SingletScalarBlocks3d qualified as SS
import Control.Monad.IO.Class                 (liftIO)
import Control.Monad.Reader                   (local)
import Data.Aeson                             (ToJSON)
import Data.Binary                            (Binary)
import Data.Proxy                             (Proxy (..))
import Data.Scientific                        (Scientific)
import Data.Text                              (Text)
import Data.Vector                            (Vector)
import GHC.Generics                           (Generic)
import Hyperion
import Hyperion.Bootstrap.BinarySearch        (BinarySearchConfig (..),
                                               Bracket (..))
import Hyperion.Bootstrap.Bound               (BinarySearchBound (..),
                                               Bound (..), BoundFiles (..))
import Hyperion.Bootstrap.Bound               qualified as Bound
import Hyperion.Bootstrap.Main                (unknownProgram)
import Hyperion.Bootstrap.Params              qualified as Params
import Hyperion.Database                      qualified as DB
import Hyperion.Log                           qualified as Log
import Hyperion.Util                          (minute, hour)
import Numeric.Rounded                        (Rounded, RoundingMode (..))
import Projects.Scalars3d.Defaults            (defaultBoundConfig)
import SDPB qualified
import SDPB                                   (Params (..))

data SingletScalarBinarySearch = SingletScalarBinarySearch
  { ssbs_bound     :: Bound Int SingletScalar
  , ssbs_config    :: BinarySearchConfig Rational
  , ssbs_gapSector :: Int
  } deriving (Show, Generic, Binary, ToJSON)

remoteSingletScalarBinarySearch :: SingletScalarBinarySearch -> Cluster (Bracket Rational)
remoteSingletScalarBinarySearch ssbs = Bound.remoteBinarySearchBound MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ssbs_bound ssbs) `cAp` cPure (ssbs_gapSector ssbs)
  , bsConfig            = ssbs_config ssbs
  , bsResultBoolClosure = cPtr (static getBool)
  }
  where
    mkBound bound gapSector gap =
      bound { boundKey = (boundKey bound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (boundKey bound))
    getBool bound files result = do
      case SDPB.isDualFeasible result of
        False -> pure ()
        True  -> saveVectors bound files result
      pure $ not (SDPB.isDualFeasible result)

-- Differs from SingletScalar in that it saves the coefficients in derivative basis
saveVectors :: Bound Int SingletScalar -> BoundFiles -> SDPB.Output -> Job ()
saveVectors bound' boundFiles result =
  Bound.reifyPrecisionWithFetchContext bound' (Proxy @Job) bound'.precision
  $ \(_ :: Proxy p) -> do
  let cftB = (bound' :: Bound Int SingletScalar) { Bound.precision = Proxy @p }
  norm <- Bound.getBoundObject @Job @SingletScalar @p @(Vector (Rounded 'TowardZero p)) cftB boundFiles $
    SDPB.normalizationVector . SDPB.normalization . Bound.toSDP
  alphaVec <- liftIO $ SDPB.readFunctional norm (outDir boundFiles)
  Log.text "Saving functional"
  DB.insert functionalVectors bound' (alphaVec, SDPB.dualObjective result)
  where
    functionalVectors :: DB.KeyValMap (Bound Int SingletScalar) (Vector a, Scientific)
    functionalVectors = DB.KeyValMap "functional_vectors"

-- Run an SDP defined in a directory containing Json files, and return
-- the result.
runSDPBJsonDir :: Bound.BoundConfig -> SDPB.Params -> FilePath -> Job SDPB.Output
runSDPBJsonDir boundConfig sdpbParams sdpJsonDir = do
  workDir <- newWorkDir sdpJsonDir
  jsonFiles <- liftIO $
    map (sdpJsonDir </>) .
    filter (\f -> takeExtension f == ".json") <$>
    listDirectory sdpJsonDir
  let
    boundFiles = (Bound.defaultBoundFiles workDir) { jsonDir = sdpJsonDir }
    sdpbInput  = Bound.sdpbInputFromBoundFiles boundFiles jsonFiles sdpbParams
  Log.info "Running SDPB" sdpbInput
  sdpbOutput <- liftIO $ SDPB.run
    (Bound.scriptsDir boundConfig </> "srun_sdp2input.sh")
    (Bound.scriptsDir boundConfig </> "srun_sdpb.sh")
    sdpbInput
  Log.info "SDPB result" (sdpJsonDir, sdpbOutput)
  pure sdpbOutput

singletScalarDefaultGaps :: Int -> Rational -> SingletScalar
singletScalarDefaultGaps nmax dPhi = SingletScalar
  { spectrum     = unitarySpectrum
  , objective    = SS.Feasibility
  , blockParams  = Params.block3dParamsNmax nmax
  , spins        = Params.spinsNmax nmax
  , deltaPhi     = dPhi
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "SingletScalarB3dAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (Bound.remoteCompute . go)
  [ (0.5181489, 1.4) -- Should be allowed
  , (0.5181489, 1.5) -- Should be disallowed
  ]
  where
    nmax = 6
    go (dPhi, deltaS) = Bound
      { boundKey = (singletScalarDefaultGaps nmax dPhi)
        { spectrum = setGap 0 deltaS unitarySpectrum
        }
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "SingletScalarB3d_binary_search_test_nmax6" =
  mapConcurrently_ search
  [ (0.5181489, 6, MPIJob 1 6, 30*minute, 768) ]
  where
    search (dPhi, nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = Bound
        { boundKey = singletScalarDefaultGaps nmax dPhi
        , precision = (Params.blockParamsNmax nmax).precision
        , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 0
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 8
          }
        , threshold  = 1e-30
        , terminateTime = Nothing
        }
      }

boundsProgram "central_charge_bound_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (60*minute) . setSlurmPartition "shared" . setJobMemory "16G") $
  mapConcurrently_ Bound.remoteComputeKeepFiles $ do
  dPhi <- [0.5181489]
  kpo <- [8, 12, 16, 20, 24]
  pure $ go kpo dPhi
  where
    nmax = 6
    go kpo dPhi = Bound
      { boundKey = SingletScalar
        { spectrum     = unitarySpectrum
        , objective    = SS.StressTensorOPEBound UpperBound
        , blockParams  = (Params.block3dParamsNmax nmax)
          { B3d.keptPoleOrder = kpo
          , B3d.precision = 1024
          }
        , spins        = Params.spinsNmax nmax
        , deltaPhi     = dPhi
        }
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "central_charge_bound_nmax10" =
  local (setJobType (MPIJob 1 128) . setJobTime (2*hour) . setSlurmPartition "compute") $
  mapConcurrently_ Bound.remoteComputeKeepFiles $ do
  dPhi <- [0.5181489]
  -- kpo <- [80, 60, 40, 20]
  kpo <- [80, 70, 60, 50, 40, 30, 20, 10]
  pure $ go kpo dPhi
  where
    nmax = 10
    go kpo dPhi = Bound
      { boundKey = SingletScalar
        { spectrum     = unitarySpectrum
        , objective    = SS.StressTensorOPEBound UpperBound
        , blockParams  = (Params.block3dParamsNmax nmax)
          { B3d.keptPoleOrder = kpo
          , B3d.order         = 80
          , B3d.precision     = 1280
          }
        , spins        = Params.spinsNmax nmax
        , deltaPhi     = dPhi
        }
      , precision = 1280
      , solverParams = (Params.optimizationParams nmax)
        { SDPB.precision           = 1280
        , SDPB.dualityGapThreshold = 1e-100
        , SDPB.dualErrorThreshold  = 1e-80
        }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "central_charge_bound_nmax10_samplePoints" =
  local (setJobType (MPIJob 1 128) . setJobTime (2*hour) . setSlurmPartition "compute") $
  mapConcurrently_ go sdpDirs
  where
    nmax = 10
    sdpbParams = (Params.optimizationParams nmax)
      { SDPB.precision           = 1280
      , SDPB.dualityGapThreshold = 1e-100
      , SDPB.dualErrorThreshold  = 1e-80
      }
    go sdpDir = remoteEvalJob $
      static runSDPBJsonDir `cAp`
      cPure defaultBoundConfig `cAp`
      cPure sdpbParams `cAp`
      cPure sdpDir
    sdpBaseDir = "/home/dsd/projects/scalars-3d/samplePoints_sdps"
    sdpDirs = (sdpBaseDir </>) <$>
      [ "scalar_cmin_nmax_10_samplePoints_110"
        -- "scalar_cmin_nmax_10_samplePoints_100"
      , "scalar_cmin_nmax_10_samplePoints_90"
      , "scalar_cmin_nmax_10_samplePoints_80"
      , "scalar_cmin_nmax_10_samplePoints_70"
      , "scalar_cmin_nmax_10_samplePoints_60"
      , "scalar_cmin_nmax_10_samplePoints_50"
      , "scalar_cmin_nmax_10_samplePoints_40"
      , "scalar_cmin_nmax_10_samplePoints_30"
      ]

boundsProgram p = unknownProgram p
