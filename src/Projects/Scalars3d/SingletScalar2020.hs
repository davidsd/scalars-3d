{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StaticPointers        #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE OverloadedRecordDot   #-}

module Projects.Scalars3d.SingletScalar2020 where

import           Blocks.Delta                    (Delta (..))
import qualified Blocks.ScalarBlocks             as SB
import           Bootstrap.Bounds.Spectrum       (setGap, unitarySpectrum)
import qualified Bootstrap.Math.DampedRational   as DR
import qualified Bootstrap.Math.VectorSpace      as VS
import           Bounds.Scalars3d.SingletScalar  (SingletScalar (..))
import qualified Bounds.Scalars3d.SingletScalar  as SS
import           Control.Monad.IO.Class          (liftIO)
import           Control.Monad.Reader            (local)
import           Data.Aeson                      (ToJSON)
import           Data.Binary                     (Binary)
import           Data.Functor.Identity           (Identity (..))
import           Data.Proxy                      (Proxy (..))
import           Data.Scientific                 (Scientific)
import           Data.Text                       (Text)
import           Data.Vector                     (Vector)
import qualified Data.Vector                     as V
import           GHC.Generics                    (Generic)
import           Hyperion
import           Hyperion.Bootstrap.BinarySearch (BinarySearchConfig (..),
                                                  Bracket (..))
import           Hyperion.Bootstrap.Bound        (BinarySearchBound (..),
                                                  Bound (..),
                                                  BoundFileTreatment (..),
                                                  BoundFiles (..),
                                                  FileTreatment (..))
import qualified Hyperion.Bootstrap.Bound        as Bound
import           Hyperion.Bootstrap.Main         (unknownProgram)
import qualified Hyperion.Bootstrap.Params       as Params
import qualified Hyperion.Database               as DB
import qualified Hyperion.Log                    as Log
import           Hyperion.Util                   (minute)
import           Numeric.Rounded                 (Rounded, RoundingMode (..))
import           Projects.Scalars3d.Defaults     (defaultBoundConfig)
import           SDPB                            (Params (..))
import qualified SDPB

data SingletScalarBinarySearch = SingletScalarBinarySearch
  { ssbs_bound     :: Bound Int SingletScalar
  , ssbs_config    :: BinarySearchConfig Rational
  , ssbs_gapSector :: Int
  } deriving (Show, Generic, Binary, ToJSON)

remoteSingletScalarBinarySearch :: SingletScalarBinarySearch -> Cluster (Bracket Rational)
remoteSingletScalarBinarySearch ssbs = Bound.remoteBinarySearchBound MkBinarySearchBound
  { bsBoundClosure      = static mkBound `ptrAp` cPure (ssbs_bound ssbs) `cAp` cPure (ssbs_gapSector ssbs)
  , bsConfig            = ssbs_config ssbs
  , bsResultBoolClosure = cPtr (static getBool)
  }
  where
    mkBound bound gapSector gap =
      bound { boundKey = (boundKey bound) { spectrum = gappedSpectrum } }
      where
        gappedSpectrum = setGap gapSector gap (spectrum (boundKey bound))
    getBool bound files result = do
      case SDPB.isDualFeasible result of
        False -> pure ()
        True  -> saveVectors bound files result [0,2,4,6,8]
      -- For an optimization, return whether the functional acting on
      -- the unit operator is less than zero.
      pure $ SDPB.isDualFeasible result && SDPB.dualObjective result < 0

-- TODO: Generalize this function away from SingletScalar
saveVectors :: Bound Int SingletScalar -> BoundFiles -> SDPB.Output -> [Int] -> Job ()
saveVectors bound' boundFiles result spins =
  Bound.reifyPrecisionWithFetchContext bound' (Proxy @Job) bound'.precision
  $ \(_ :: Proxy p) -> do
  let cftB = (bound' :: Bound Int SingletScalar) { Bound.precision = Proxy @p }
  norm <- Bound.getBoundObject @Job @SingletScalar @p @(Vector (Rounded 'TowardZero p)) cftB boundFiles $
    SDPB.normalizationVector . SDPB.normalization . Bound.toSDP
  alphaVec <- liftIO $ SDPB.readFunctional norm (outDir boundFiles)
  let getVec l = Bound.getBoundObject cftB boundFiles $
        SS.getInternalBlockVec (SB.SymTensorRep (RelativeUnitarity 0) l)
      getAlphaAction = fmap (DR.mapNumerator (Identity . dotPols alphaVec)) . getVec
  alphaActions <- traverse getAlphaAction spins
  Log.info "Functional" $ zip spins alphaActions
  mapM_ (\(l,f) -> DB.insert functionalActions (bound',l) (f, SDPB.dualObjective result)) $
    zip spins alphaActions
  where
    dotPols v pv = VS.sum $ V.zipWith (VS.*^) v pv
    functionalActions :: DB.KeyValMap (Bound Int SingletScalar, Int) (DR.DampedRational base Identity a, Scientific)
    functionalActions = DB.KeyValMap "functional_actions"

singletScalarDefaultGaps :: Int -> Rational -> SingletScalar
singletScalarDefaultGaps nmax dPhi = SingletScalar
  { spectrum     = unitarySpectrum
  , objective    = SS.Feasibility
  , blockParams  = Params.blockParamsNmax nmax
  , spins        = Params.spinsNmax nmax
  , deltaPhi     = dPhi
  }

boundsProgram :: Text -> Cluster ()

boundsProgram "SingletScalarAllowed_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (0.5181489, 1.4) -- Should be allowed
  , (0.5181489, 1.5) -- Should be disallowed
  ]
  where
    nmax = 6
    bound (deltaPhi, deltaS) = Bound
      { boundKey = (singletScalarDefaultGaps nmax deltaPhi)
        { spectrum = setGap 0 deltaS unitarySpectrum
        }
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

-- | The lower bound on cT should be something like -Delta_sig^2 /
-- objective, in units where cT_free = 1.
boundsProgram "SingletScalar_cT_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (Bound.remoteCompute . bound)
  [ (0.5181489, 1.412625) ]
  where
    nmax = 6
    bound (deltaPhi, deltaS) = Bound
      { boundKey = SingletScalar
        { spectrum     = setGap 0 deltaS unitarySpectrum
        , objective    = SS.CTLowerBound
        , blockParams  = Params.blockParamsNmax nmax
        , spins        = Params.spinsNmax nmax
        , deltaPhi     = deltaPhi
        }
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.optimizationParams nmax) { SDPB.precision = 768 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "SingletScalarDirty_test_nmax6" =
  local (setJobType (MPIJob 1 6) . setJobTime (25*minute)) $
  mapConcurrently_ (Bound.remoteComputeWithFileTreatment
                     (Bound.keepAllFiles { sdpDirTreatment = RemoveFile })
                     Bound.defaultBoundFiles . bound)
  [ -- (0.5181489, 1.4) -- Should be allowed
  -- ,
    (0.5181489, 1.5) -- Should be disallowed
  ]
  where
    nmax = 6
    bound (deltaPhi, deltaS) = Bound
      { boundKey = (singletScalarDefaultGaps nmax deltaPhi)
        { spectrum = setGap 0 deltaS unitarySpectrum
        }
      , precision = (Params.blockParamsNmax nmax).precision
      , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = 768, verbosity = 2 }
      , boundConfig = defaultBoundConfig
      }

boundsProgram "SingletScalar_binary_search_test_nmax6" =
  mapConcurrently_ search
  [ (0.5181489, 6, MPIJob 1 6, 30*minute, 768) ]
  where
    search (deltaPhi, nmax, jobType, jobTime, prec) =
      local (setJobType jobType . setJobTime jobTime) $
      remoteSingletScalarBinarySearch $
      SingletScalarBinarySearch
      { ssbs_bound = Bound
        { boundKey = singletScalarDefaultGaps nmax deltaPhi
        , precision = (Params.blockParamsNmax nmax).precision
        , solverParams = (Params.jumpFindingParams nmax) { SDPB.precision = prec }
        , boundConfig = defaultBoundConfig
        }
      , ssbs_gapSector = 0
      , ssbs_config = BinarySearchConfig
        { initialBracket = Bracket
          { truePoint  = 1
          , falsePoint = 8
          }
        , threshold  = 1e-30
        , terminateTime = Nothing
        }
      }


boundsProgram p = unknownProgram p
