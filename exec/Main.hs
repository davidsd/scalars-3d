module Main where

import           Config                         (config, staticConfig)
import           Hyperion.Bootstrap.Main        (hyperionBootstrapMain,
                                                 tryAllPrograms)
import qualified Projects.Scalars3d.IsingSigEpsTest2020
import qualified Projects.Scalars3d.SingletScalar2020
import qualified Projects.Scalars3d.SingletScalarBlocks3d2020
import qualified Projects.Scalars3d.ONVec2021

main :: IO ()
main = hyperionBootstrapMain config staticConfig $
  tryAllPrograms
  [ Projects.Scalars3d.IsingSigEpsTest2020.boundsProgram
  , Projects.Scalars3d.SingletScalar2020.boundsProgram
  , Projects.Scalars3d.SingletScalarBlocks3d2020.boundsProgram
  , Projects.Scalars3d.ONVec2021.boundsProgram
  ]
